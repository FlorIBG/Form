var config = {
  apiKey: "AIzaSyCSK2UEebfIgmbHSytyPHof2AcnQOiX0ho",
  authDomain: "caracol-74d6b.firebaseapp.com",
  databaseURL: "https://caracol-74d6b.firebaseio.com",
  projectId: "caracol-74d6b",
  storageBucket: "caracol-74d6b.appspot.com",
  messagingSenderId: "164546816395"
};

var firebaseApp =  firebase.initializeApp(config);
var slide = firebase.database().ref('sliders');
var db = firebaseApp.database();

console.log(slide);

var editable_slider = new Vue({
  el: '#editable_slider',
  data: {
    words: [],
    item_on_edit: {
        href: '',
        img: {
            src: '',
            alt: '',
            title: ''
        },
        text: ''
    },
    is_edit: false,
    time: undefined,
  },
  firebase: {
    slide: db.ref('sliders')
  },
  mounted () {
    // this.item_on_edit = this.edit_empty();
    this.plays();
  },
  created: function() {
    this.loadWords()
  },
  methods: {
    loadWords: function() {
      axios.get('slider/js/words.json').then(response => {
        this.words = response.data
      })
    },
    // edit: function(word) {
    //   if (this.is_edit) {
    //     return alert('no puedes editar otra tarea');
    //   }
    //
    //   this.item_on_edit = this.$firebaseRefs.slide.child(word);
    //   // this.eliminar(childKey);
    //   this.is_edit = true
    // },
    edit: function(post) {
        // Set post values to form
        this.item_on_edit = post
    },
    update: function(post) {
       const childKey = post['.key'];
       /*
        * Firebase doesn't accept speacial chars as value
        * so delete `.key` property from the post
        */
       delete post['.key'];
       /*
        * Set the updated post value
        */
       this.$firebaseRefs.slide.child(childKey).set(post)
    },
    eliminar: function(key) {
      // this.words.splice(index,1);
      // slide.child(index['.key']).remove();
      this.$firebaseRefs.slide.child(key).remove()
    },
    create: function() {
      if (!this.is_valid){
        return this.alerta()
      }
      this.is_edit = false

      // this.words.push(this.item_on_edit)
      slide.push(this.item_on_edit)
      this.clear_inputs()

    },
    edit_empty: function() {
      return {
        href: '',
        img: {
            alt: '',
            src: '',
            title: ''
        },
        text: ''
      }
    },
    alerta: function() {
      alert("No has ingresado ninguna tarea :( o tus url no son tipo url");
    },
    task_empty: function(text) {
      if(text === undefined || text.trim() == '') {
        return false;
      }
      return true
    },
    type_url: function(url) {
      var pattern = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;

      if (!pattern.test(url)) {
        return false;
      }
      return true;
    },
    clear_inputs: function () {
      this.item_on_edit = this.edit_empty()
    },
    validate: function () {
      return this.is_title && this.is_alt && this.is_image && this.is_href && this.is_text;
    },
    next: function(){
      var first = this.words.shift()
      this.words.push(first)
    },
    prev: function(){
        var last = this.words.pop()
        this.words.unshift(last)
    },
    plays: function(){
      this.time= setInterval(this.next, 3000);
    },
    pause: function(){
      this.time= clearInterval(this.time);
    }
  },
  computed: {
    is_title: function () {
      return this.task_empty(this.item_on_edit.img.title);
    },
    is_alt: function () {
      return this.task_empty(this.item_on_edit.img.alt);
    },
    is_image: function() {
      return this.type_url(this.item_on_edit.img.src);
    },
    is_href: function() {
      return this.type_url(this.item_on_edit.href);
    },
    is_text: function () {
        return this.task_empty(this.item_on_edit.text);
    },
    is_valid: function() {
      return this.validate();
    }

  }
})
