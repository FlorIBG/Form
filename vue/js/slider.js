var excercise = new Vue({
  el: '#arraycito',
  data: {
    arraycito: ['a','b','c','d','e','f'],
    time: undefined,
    imgs: [
      { url: 'https://images3.alphacoders.com/621/621682.jpg',
        alt: '',
        txt: 'saas'
      },
      { url: 'http://minimal-wallpapers.com/wp-content/uploads/2015/09/giraffe-minimalist-wallpaper-orange.png',
        alt: '',
        txt: 'saas'
      },
      { url: 'https://wallpaper.wiki/wp-content/uploads/2017/05/Pictures-download-abstract-minimalist-wallpaper-HD.jpg',
        alt: '',
        txt: 'sasa'
      },
      { url: 'http://www.androidguys.com/wp-content/uploads/2016/05/tumblr_static_summer_sunset_created_by_-_marilou.png',
        alt: '',
        txt: 'sasas'
      }
    ]
  },
  mounted () {
    this.play();
    console.log('holi');
  },
  methods: {
    next: function(){
      // var shift = this.arraycito.shift()
      // var replace = this.arraycito.splice(1, 1, shift);
      // this.arraycito.push(this.arraycito.splice(this.arraycito.indexOf('f'), 6)[0]);
      var first = this.imgs.shift()
      // console.log(first);
      this.imgs.push(first)

    },
    prev: function(){
      console.log('prev')
      // for (var i = 0; i < this.arraycito.length; i++) {
        var last = this.imgs.pop()
        this.imgs.unshift(last)
      // }
      // var pop = this.arraycito.pop()
      //var replace = this.arraycito.splice(5, 1, pop);
      // this.arraycito.push(this.arraycito.splice(this.arraycito.indexOf('a'), 1)[0]);
    },
    play: function(){
      this.time= setInterval(this.next, 3000);
    },
    pause: function(){
      this.time= clearInterval(this.time);
    }
  },
})
