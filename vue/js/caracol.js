var caracol = Vue.component('caracol',{
    template: '#caracol-template',
    props:['size'],
    computed: {
        styles: function() {
            return {
                width: (this.size.width + 'px'),
                height: (this.size.height + 'px'),
                background:this.getRandomColor,
            }

         },
         getRandomColor: function() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        },
        square: function() {
            return {
                width: this.size.squareWidth + 'px',
                height:  this.size.squareHeight + 'px',
                // background: 'darkmagenta',
                marginLeft: this.size.right === 0 ? 'auto' : 0,
                marginRight: this.size.left === 0 ? 'auto' : 0
            }
        }
    },
})

// var square = Vue.component('square',{
//     template: '#square-template',
//     props: ['square'],
//     computed: {
//          getRandomColor: function() {
//             var letters = '0123456789ABCDEF';
//             var color = '#';
//             for (var i = 0; i < 6; i++) {
//                 color += letters[Math.floor(Math.random() * 16)];
//             }
//             return color;
//         },
//         styles: function() {
//             return {
//                 width: (this.square.width + 'px'),
//                 height: (this.square.height + 'px'),
//                 background:this.getRandomColor,
//             }
//         }
//     },
// });

new Vue({
    el: '#container',
    data: {
        rectangles:10,
        container:600,
        sizes: [],
        cuadrados: []
    },
    ready() {
        console.log(this.squares);
    },
    computed: {
        medidas: function() {
            for (var i = 1; i <= this.rectangles; i++) {

                var height = this.container/this.formula;
                var width = this.container;

                this.sizes.push({
                    height: i%2 === 0 ? width : height,
                    width: i%2 === 0 ? height : width,
                    right: i%4 === 1 ? 0 : '',
                    top: i%4 === 2 ? 0 : '',
                    bottom: i%4 === 3 ? 0 : '',
                    left: i%4 === 0 ? 0 : '',
                    squareHeight: height,
                    squareWidth: height
                });

                width = height - this.container;
                this.container = height;
            }
        },
        // squares: function() {
        //     for (var i = 1; i < this.sizes; i++) {
        //         var width =  this.sizes[i].height;
        //
        //         var height = this.sizes[i].height;
        //
        //         this.cuadrados.push({
        //             height: height,
        //             width: width
        //         });
        //
        //
        //     }
        // },
        formula: function() {
            return ((1 + Math.sqrt(5)) / 2);
        },
    },
})
