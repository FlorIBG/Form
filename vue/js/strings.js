// var strings = new Vue({
//   el: '#strings',
//   data: {
//     words: [],
//     item_on_edit: {},
//     is_edit: false,
//   },
//   mounted () {
//     this.item_on_edit = this.edit_empty();
//   },
//   created: function() {
//     this.loadWords()
//   },
//   methods: {
//     loadWords: function() {
//       axios.get('vue/js/words.json').then(response => {
//         this.words = response.data
//       })
//     },
//     edit: function(index) {
//       if (this.is_edit) {
//         return alert('no puedes editar otra tarea');
//       }
//       this.item_on_edit = this.words[index];
//       this.eliminar(this.item);
//       this.is_edit = true
//     },
//     eliminar: function(index) {
//       this.words.splice(index,1);
//     },
//     create: function() {
//       if (!this.is_valid) {
//         return this.alerta()
//       }
//       this.is_edit = false
//       this.words.push(this.item_on_edit)
//       this.clear_inputs()
//
//     },
//     edit_empty: function() {
//       return {}
//     },
//     alerta: function() {
//       alert("No has ingresado ninguna tarea :(");
//     },
//     task_empty: function() {
//       if(this.item_on_edit.text === undefined || this.item_on_edit.text.trim() == '') {
//         return false;
//       }
//       return true
//     },
//     clear_inputs: function () {
//       this.item_on_edit = this.edit_empty()
//     },
//     return_task: function () {
//
//     }
//   },
//   computed: {
//     is_valid: function () {
//       return this.task_empty();
//     }
//   }
// })
var strings = new Vue({
  el: '#strings',
  data: {
    words: [],
    item_on_edit: {
      href: '',
      img: {
        src: '',
        alt: '',
        title: ''
      }
    },
    is_edit: false,
  },
  mounted () {
    this.item_on_edit = this.edit_empty();
  },
  created: function() {
    this.loadWords()
  },
  methods: {
    loadWords: function() {
      axios.get('vue/js/words.json').then(response => {
        this.words = response.data
      })
    },
    edit: function(index) {
      if (this.is_edit) {
        return alert('no puedes editar otra tarea');
      }
      this.item_on_edit = this.words[index];
      this.eliminar(index);
      this.is_edit = true
    },
    eliminar: function(index) {
      this.words.splice(index,1);
    },
    create: function() {
      if (this.is_valid){
        return this.alerta()
      }
      this.is_edit = false

      this.words.push(this.item_on_edit)
      this.clear_inputs()

    },
    edit_empty: function() {
      return {
        href: '',
        img: {
          src: '',
          alt: '',
          title: ''
        }
      }
    },
    alerta: function() {
      alert("No has ingresado ninguna tarea :( o tus url no son tipo url");
    },
    task_empty: function(text) {
      // var text = document.querySelector('.text_type').value
      if(text === undefined || text.trim() == '') {
        return false;
      }
      return true
    },
    type_url: function(url) {
      var pattern = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;

      if (!pattern.test(url)) {
        return false;
      }
      return true;
    },
    clear_inputs: function () {
      this.item_on_edit = this.edit_empty()
    },
    validate: function () {
      return !this.is_title || !this.is_alt || !this.is_url || !this.is_href;
    }
  },
  computed: {
    is_title: function () {
      return this.task_empty(this.item_on_edit.img.title);
    },
    is_alt: function () {
      return this.task_empty(this.item_on_edit.img.alt);
    },
    is_image: function() {
      return this.type_url(this.item_on_edit.img.src);
    },
    is_href: function() {
      return this.type_url(this.item_on_edit.href);
    },
    is_valid: function() {
      return this.validate();
    }

  }
})
