
<style media="screen">
  .clase {
    width: 10px; height: 10px; background-color: blue; margin-left: auto; margin-right: auto; position: relative;
  }
  .row {
    width: 100%; background-color: purple; display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-around;  margin-top: 5%; align-items: center; min-height: 10px;
  }
  .text {
    font-size: 10px; text-align: center; color: white; position: absolute;
    top: -100%;
    left: 30%;
  }
</style>

<?php

  function Square($n)
  {
     ?>
     <div class="clase">
       <p class="text"><?php echo $n ?></p>
     </div>
    <?php
  }
    Square(5);

  function Row(array $squares)
  {
    ?>
      <div class="row">
        <?php foreach ($squares as $n): ?>
          <?php echo Square($n); ?>
        <?php endforeach; ?>
      </div>
    <?php
  }
  Row([5,4,6]);

  $rows = getRows(100);

  foreach ($rows as $row) {
    Row($row);
  }

  function getRows (int $n) {
    $rows = [];
    $rows[] = [1];

    for ($i=1; $i <= $n; $i++) {
      $rows[] = nextRow($rows[$i-1]);
    }

    return $rows;
  }

  function nextRow(array $row) {
    $next_row = [];

    $length = count($row);
    $next_row[] = 1;

    for ($i=1; $i < $length ; $i++) {
      $before = $row[$i];
      $after =  $row[$i-1];
      $next_row[$i] = $before + $after;
    }

    $next_row[$length] = 1;

    return $next_row;
  }


 ?>
